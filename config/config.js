const DATABASE          = 'dk_tpi2';
const DATABASE_USERNAME = 'root';
const DATABASE_PASSWORD = '';
const DATABASE_HOST     = 'localhost';
const SERVER_PORT       = '8000';

exports.db = DATABASE;
exports.db_username = DATABASE_USERNAME;
exports.db_password = DATABASE_PASSWORD;
exports.db_host = DATABASE_HOST;
exports.server_port = SERVER_PORT;