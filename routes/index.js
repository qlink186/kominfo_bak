let express = require('express');
let router = express.Router() ;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render( 'portal/index', {
    title:  'Pemerintah Kota Tanjungpinang',
    menu:   './'
    });
});

module.exports = router;
