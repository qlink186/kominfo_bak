let express = require('express');
let router = express.Router() ;
let Member = require('../../model/member');

router.get('/', isAuthenticated, (req, res, next) => {
    res.render( 'adm/adm-member-list', {
        menu:   'adm-member-list',
        title:  'Data Member',
        desc:   'Kelola Member berdasarkan level member.'
    });
});

router.get('./logout', function(req, res){
    req.logout();
    req.flash('success_msg', 'Anda telah berhasil keluar');
    res.redirect('./auth');
});

function isAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect('./auth');
    }
}

module.exports = router;