let express = require('express');
let router = express.Router() ;

router.get('/', isAuthenticated, (req, res, next) => {
    res.render( 'adm/adm-download-list', {
        menu:   'adm-download-list',
        title:  'Data Download | File',
        desc:   'Seluruh Dokumen yang diunggah oleh member'
    });
});

router.get('./logout', function(req, res){
    req.logout();
    req.flash('success_msg', 'Anda telah berhasil keluar');
    res.redirect('./auth');
});

function isAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect('./auth');
    }
}
module.exports = router;