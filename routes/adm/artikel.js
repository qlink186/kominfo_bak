let express = require('express');
let router = express.Router();

router.get('/', isAuthenticated, (req, res, next) => {
    res.render( 'adm/adm-artikel-list', {
        menu:   'adm-artikel-list',
        title:  'Data Artikel',
        desc:   'Pengelolaan Artikel / Berita di Lingkungan Pemerintah Kota Tanjungpinang'
    });
});

router.get('/logout', function(req, res){
    req.logout();
    req.flash('success_msg', 'Anda telah berhasil keluar');
    res.redirect('./auth');
});

function isAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect('/auth');
    }
}

module.exports = router;