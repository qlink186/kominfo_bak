let express = require('express');
let router = express.Router() ;

router.get('/', isAuthenticated, (req, res, next) => {
    res.render( 'adm/adm-aduan', {
        menu:   'adm-aduan-list',
        title:  'Data Aduan Masyarakat',
        desc:   'Pelaporan Masyarakat terhadap kinerja Unit Kerja di Lingkungan Pemerintah Kota Tanjungpinang'
    });
});

router.get('./logout', function(req, res){
    req.logout();
    req.flash('success_msg', 'Anda telah berhasil keluar');
    res.redirect('./auth');
});

function isAuthenticated(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }else{
        res.redirect('./auth');
    }
}
module.exports = router;