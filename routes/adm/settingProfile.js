let express = require('express');
let router = express.Router();

router.get('/', isAuthenticated, (req, res, next) => {
    res.render( 'adm/adm-setting-profile', {
        menu:   'adm-setting-profile',
        title:  'Profile',
        desc:   'Pengaturan Profil Member'
    });
});

router.get('./adm/logout', function (req, res) {
    req.logout();
    req.flash('success_msg', 'Anda telah berhasil keluar');
    res.redirect('./auth');
});

function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect('/auth');
    }
}

module.exports = router;