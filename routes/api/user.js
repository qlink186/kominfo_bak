const express = require('express');
const router = express.Router();
const Joi = require('joi');

const path = require('path');     //used for file path
const crypto = require("crypto");
const multer = require('multer');
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err);
            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
});
const upload = multer({ storage: storage });

const Member = require('../../model/member');

router.post('/login', (req, res, next) => { //login
    const schema = Joi.object().keys({
        username: Joi.string().required(),
        password: Joi.string().required()
    });
    Joi.validate(req.body, schema, (err, value)=>{
        if(err) {
            res.status(400).json(err.details[0].message);
        }else{
            Member.findOne({
                where:{
                    username:req.body.username
                }
            }).then((result) => {
                if(!result) {
                    res.status(400).json('User not found');
                }else{
                    Member.comparePassword(req.body.password,result.get('password'), (err,isMatch) => {
                        if(err) throw err;
                        if(isMatch){
                            res.json(result.get());
                        }else{
                            res.status(400).json('Invalid password');
                        }
                    });
                }
            });
        }
    });
});

router.post('/', (req, res, next) => { //register
    const schema = Joi.object().keys({
        username: Joi.string().alphanum().min(3).required(),
        password: Joi.string().min(6).required(),
        email: Joi.string().email(),
        full_name: Joi.string().required(),
        identityNumber: Joi.number().required(),
        identity: Joi.string().required(),
        phone: Joi.number().required(),
        address: Joi.string(),
    });
    Joi.validate(req.body, schema, (err, body) => {
        if(err) {
            res.status(400).json(err.details[0].message);
        }else{
            Member.createUser(body, (ok) => {
                res.json("Success");
            }, (err) => {
                console.log(err);
                res.status(400).json(err.errors[0].message);
            });
        }
    });
});

router.put('/:username', (req, res, next) => { //update profile
    const schema = Joi.object().keys({
        email: Joi.string().email(),
        full_name: Joi.string().required(),
        identityNumber: Joi.number().required(),
        identity: Joi.string().required(),
        phone: Joi.number().required(),
        address: Joi.string(),
    });
    Joi.validate(req.body, schema, (err, body) => {
        if(err) {
            res.status(400).json(err.details[0].message);
        }else{
            Member.findOne({
                where:{
                    username:req.params.username
                }
            }).then((result)=>{
                if(!result) {
                    res.status(400).json("User not found");
                }else{
                    Member.update(body,{
                        where:{
                            username:req.params.username
                        }
                    }).then((success)=>{
                        res.json("Success");
                    }).catch((err)=>{
                        console.log(err);
                        res.status(400).json(err.errors[0].message);
                    });
                }
            });
        }
    });
});

router.post('/photo/:username', upload.single('file'),(req, res, next) => { //update photo profile
    Member.findOne({
        where:{
            username:req.params.username
        }
    }).then((result)=>{
        if(!result) {
            res.status(400).json("User not found");
        }else{
            Member.update({
                avatar:req.file.filename
            },{
                where:{
                    username:req.params.username
                }
            }).then((success)=>{
                res.json("Success");
            }).catch((err)=>{
                console.log(err);
                res.status(400).json(err.errors[0].message);
            });
        }
    });
});

module.exports = router;