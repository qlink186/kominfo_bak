const express = require('express');
const router = express.Router();
const Joi = require('joi');

const path = require('path');     //used for file path
const crypto = require("crypto");
const multer = require('multer');
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err);
            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
});
const upload = multer({ storage: storage });

const Complain = require('../../model/complain');
const Category = require('../../model/category');

router.post('/', (req, res, next) => {
    const schema = Joi.object().keys({
        author_id: Joi.number().required(),
        title: Joi.string().required(),
        content: Joi.string().required(),
        location: Joi.string().required(),
        category_id: Joi.number().required(),
        instansi_id: Joi.number(),
        instansi: Joi.number(),
        rating: Joi.number(),
        note: Joi.string(),
    });
    Joi.validate(req.body, schema, (err, body) => {
        if(err) {
            res.status(400).json(err.details[0].message);
        }else{
            Complain
                .create(body)
                .then(()=>{
                    res.json("Success");
                })
                .catch((err)=>{
                    console.log(err);
                    res.status(400).json(err.errors[0].message)
                });
        }
    });
});

router.get('/',  (req, res, next) => {
    Complain
        .findAll()
        .then((rows)=>{
            res.json(rows);
        })
        .catch((err)=>{
            console.log(err);
            res.status(400).json(err.errors[0].message)
    });
});

router.get('/:id', (req, res, next) => {
    Complain
        .findAll({
            where:{
                id: req.params.id
            }
        })
        .then((rows)=>{
            res.json(rows);
        })
        .catch((err)=>{
            console.log(err);
            res.status(400).json(err.errors[0].message)
        });
});

router.put('/:id', (req, res, next) => {
    const schema = Joi.object().keys({
        rating: Joi.number().required()
    });
    Joi.validate(req.body, schema, (err, body) => {
        if(err) {
            res.status(400).json(err.details[0].message);
        }else{
            Complain.update(body,{
                where:{
                    id:req.params.id
                }
            }).then((success)=>{
                res.json("Success");
            }).catch((err)=>{
                console.log(err);
                res.status(400).json(err.errors[0].message);
            });
        }
    });
});

router.get('/owner/:username', (req, res, next) => {
    Complain
        .findAll({
            where:{
                author: req.params.username
            }
        })
        .then((rows)=>{
            res.json(rows);
        })
        .catch((err)=>{
            console.log(err);
            res.status(400).json(err.errors[0].message)
        });
});

router.get('/category', (req, res, next) => {
    Category
        .findAll()
        .then((rows)=>{
            res.json(rows);
        })
        .catch((err)=>{
            console.log(err);
            res.status(400).json(err.errors[0].message)
        });
});

module.exports = router;