let express = require('express');
let router = express.Router();

let Member = require('../../model/member');
let Media = require('../../model/media');
let Aduan = require('../../model/complain');
let AduanMedia = require('../../model/complain_media');
let Category = require('../../model/category');
let Comment = require('../../model/comment');

router.get('/', function(req, res, next) {
    Member.sync({force:true});
    Media.sync({force:true});
    Aduan.sync({force:true});
    AduanMedia.sync({force:true});
    Category.sync({force:true});
    Comment.sync({force:true});
    res.send('sync table success');
});

module.exports = router;