const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Model = sequelize.define('tb_complain_media', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    media_id:{
        type: Sequelize.INTEGER,
        allowNull: false
    },
    complain_id:{
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

module.exports = Model;