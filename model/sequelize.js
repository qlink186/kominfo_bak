const config = require('../config/config');
const Sequelize = require('sequelize');
module.exports = new Sequelize(config.db, config.db_username, config.db_password, {
    host: config.db_host,
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        underscored: false,
        freezeTableName: true,
        charset: 'utf8',
        dialectOptions: {
            collate: 'utf8_general_ci'
        },
        timestamps: false
    }
});