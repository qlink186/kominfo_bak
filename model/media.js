const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Model = sequelize.define('tb_media', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    fileName:{
        type: Sequelize.STRING,
        allowNull: false
    },
    title:{
        type: Sequelize.STRING
    },
    active:{
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
    }
},{
    timestamps:true
});

module.exports = Model;