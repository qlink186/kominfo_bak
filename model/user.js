const Sequelize = require('sequelize');
const sequelize = require('./sequelize');
const md5 = require('md5');

const Model = sequelize.define('d_users', {
    id_user: {
        type: Sequelize.INTEGER(5),
        primaryKey: true,
        autoIncrement: true,
    },
    paswd:{
        type: Sequelize.STRING(100)
    },
    username:{
        type: Sequelize.STRING(100),
        unique: true
    },
    id_peg:{
        type: Sequelize.STRING(18)
    },
    id_hakakses:{
        type: Sequelize.INTEGER(2)
    },
    tgl_daftar:{
        type: Sequelize.DATE
    },
    status_user:{
        type: Sequelize.ENUM('0','1'),
        defaultValue: '0',
    }
});

module.exports = Model;

module.exports.createUser = function(newUser, callback, error){
    newUser.paswd = md5(newUser.paswd);
    Model.create(newUser).then(callback).catch(error);
};

module.exports.comparePassword = function(candidatePassword, hash, callback){
    if( md5(candidatePassword) === hash  ){
        callback(null, true);
    }else{
        callback(null, false);
    }
};