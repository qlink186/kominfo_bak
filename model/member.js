const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize');
const uuidV4 = require('uuid/v4');

const Model = sequelize.define('v_users_active', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    username:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password:{
        type: Sequelize.STRING,
        allowNull: false
    },
    email:{
        type: Sequelize.STRING,
        unique: true
    },
    active:{
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
    },
    full_name:{
        type: Sequelize.STRING,
        allowNull: false
    },
    identity:{
        type: Sequelize.ENUM('KTP', 'SIM'),
        defaultValue: "KTP",
        allowNull: false
    },
    identityNumber:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    phone:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    avatar:{
        type: Sequelize.STRING
    },
    address:{
        type: Sequelize.STRING
    },
    token:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        defaultValue: uuidV4()
    }
},{
    timestamps:true
});

module.exports = Model;

module.exports.createUser = function(newUser, callback, error){
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newUser.password, salt, function(err, hash) {
            newUser.password = hash;
            Model.create(newUser).then(callback, error);
        });
    });
};

module.exports.comparePassword = function(candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
        if(err) throw err;
        callback(null, isMatch);
    });
};