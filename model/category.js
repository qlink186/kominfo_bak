const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Model = sequelize.define('tb_category', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    category: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT
    }
});

module.exports = Model;