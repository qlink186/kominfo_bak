const Sequelize = require('sequelize');
const sequelize = require('./sequelize');

const Model = sequelize.define('tb_complains', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    author_id:{
        type: Sequelize.INTEGER,
        allowNull: false
    },
    author:{
        type: Sequelize.STRING,
        allowNull: false
    },
    title:{
        type: Sequelize.STRING,
        allowNull: false
    },
    content     : Sequelize.TEXT,
    location    : Sequelize.STRING,
    category_id : Sequelize.INTEGER,
    category    : Sequelize.STRING,
    instansi_id : Sequelize.INTEGER,
    instansi    : Sequelize.STRING,
    rating      : Sequelize.INTEGER,
    note        : Sequelize.TEXT,
    status:{
        type: Sequelize.ENUM('Draft', 'Diteruskan','Ditanggapi','Dihapus'),
        defaultValue: "Draft",
        allowNull: false
    },
    public:{
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false
    }
},{
    timestamps:true
});

module.exports = Model;