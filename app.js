let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let lessMiddleware = require('less-middleware');

let session = require('express-session');
let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;
let flash = require('connect-flash');

let app = express();

app.use(session({
    secret: 'lapor',
    resave: true,
    saveUninitialized: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Connect Flash
app.use(flash());

// Global Vars
app.use(function(req, res, next){
    let base_url = 'http://'+req.headers.host+'/';
    let adm_url = base_url+'adm/';
    res.locals.base_url = base_url;
    res.locals.module = '';
    res.locals.adm_url = adm_url;
    res.locals.menus = [
        {
            'title' : 'Dashboard',
            'link'  : '',
            'icon'  : 'dashboard'
        },{
            'title' : 'Users',
            'link'  : 'users',
            'icon'  : 'account_circle'
        }
    ];

    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded(
    { extended: true,
        limit: '50mb',
        parameterLimit: 1000000
    }));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/',                        require('./routes/index'));
app.use('/users',                   require('./routes/users'));

app.use('/auth',                    require('./routes/adm/auth'));
app.use('/adm',                     require('./routes/adm/index'));
app.use('/adm-aduan-list',          require('./routes/adm/aduan'));
app.use('/adm-download-list',       require('./routes/adm/downloads'));
app.use('/adm-gallery-gambar-list', require('./routes/adm/gallery'));
app.use('/adm-member-list',         require('./routes/adm/members'));
app.use('/adm-artikel-list',        require('./routes/adm/artikel'));
app.use('/adm-artikel-tulis',       require('./routes/adm/artikelTulis'));
app.use('/adm-artikel-kategori',    require('./routes/adm/artikelKategori'));
app.use('/adm-artikel-editor',      require('./routes/adm/artikelEditor'));
app.use('/adm-setting-profile',     require('./routes/adm/settingProfile'));
app.use('/adm/users',               require('./routes/adm/users'));

app.use('/api/tes',                 require('./routes/api/tes'));
app.use('/api/migration',           require('./routes/api/migration'));

app.use('/api/user',                require('./routes/api/user'));
app.use('/api/aduan',               require('./routes/api/aduan'));

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
//const swaggerDocument = require('./swagger.json');
const swaggerDocument = YAML.parseFile('./swagger.yaml');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
